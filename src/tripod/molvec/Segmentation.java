package tripod.molvec;

import java.util.Collection;
import tripod.molvec.*;

/**
 * A generic segmentation interface
 */
public interface Segmentation {
    Collection<Zone> getZones ();
}
